provider "aws" {
  alias   = "discord_bot"
  region  = "ap-southeast-2"
  version = "~> 1.32"

  assume_role {
    role_arn = "${lookup(var.bot_account, "deployer_role_arn")}"
  }
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}
