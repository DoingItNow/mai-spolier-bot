data "aws_vpc" "application_vpc" {
  provider = "aws.discord_bot"

  tags = {
    Name = "${lookup(var.bot_account, "vpc.app.name", "vpc-terraria")}"
  }
}

data "aws_subnet" "app_private_subnet" {
  provider = "aws.discord_bot"

  vpc_id            = "${data.aws_vpc.application_vpc.id}"
  availability_zone = "ap-southeast-2a"

  tags = {
    Name = "terraria-subnet-2a"
  }
}

# data "aws_ami" "ec2_ami" {
#   provider    = "aws.discord_bot"
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["${lookup(var.bot_account, "ec2.app.ami_name")}"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   filter {
#     name   = "root-device-type"
#     values = ["ebs"]
#   }
# }

data "template_file" "ec2_userdata" {
  template = "${file(format("%s/%s", var.env_dir, lookup(var.bot_account, "ec2.app.userdata_script")))}"
}
