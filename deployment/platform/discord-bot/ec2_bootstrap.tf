resource "aws_security_group" "temp_jenkins_sg" {
  provider = "aws.discord_bot"

  name        = "discord-bot-${lookup(var.environment_tags, "Environment")}-ec2-sg"
  description = "Provides a base level access for the platform"
  vpc_id      = "${data.aws_vpc.application_vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    description = "Outbound access to internet"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH Access"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "Incoming HTTP traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    description = "Incoming HTTPS Traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.overall_tags, var.environment_tags, var.sg_tags, map("Name", format("%s", "discord-bot-${lookup(var.environment_tags, "Environment")}-ec2-sg")))}"
}

resource "aws_iam_instance_profile" "bootstrap_profile" {
  provider = "aws.discord_bot"
  name     = "discord_bot_profile"
  role     = "${aws_iam_role.bootstrap_role.name}"
}

resource "aws_iam_role" "bootstrap_role" {
  provider = "aws.discord_bot"
  name     = "discord_bot_role"
  path     = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "bootstrap_policy" {
  provider    = "aws.discord_bot"
  name        = "discord-bot-policy"
  description = "A discord bot policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ] 
}
EOF
}

resource "aws_iam_role_policy_attachment" "bootstrap_attach" {
  provider   = "aws.discord_bot"
  role       = "${aws_iam_role.bootstrap_role.name}"
  policy_arn = "${aws_iam_policy.bootstrap_policy.arn}"
}

module "temp_jenkins_ec2_server" {
  source = "../../modules/compute/ec2"

  providers {
    aws = "aws.discord_bot"
  }

  name           = "${lookup(var.bot_account, "ec2.app.ec2_name")}"
  instance_count = "${lookup(var.bot_account, "ec2.app.instance_count")}"

  ami                         = "ami-07a3bd4944eb120a0"
  instance_type               = "${lookup(var.bot_account, "ec2.app.instance_type")}"
  key_name                    = "${lookup(var.bot_account, "ec2.app.key_name")}"
  monitoring                  = "${lookup(var.bot_account, "ec2.app.monitoring", true)}"
  associate_public_ip_address = "${lookup(var.bot_account, "ec2.app.associate_public_ip_address", false)}"
  vpc_security_group_ids      = ["${aws_security_group.temp_jenkins_sg.id}"]
  subnet_id                   = "${data.aws_subnet.app_private_subnet.id}"
  user_data                   = "${data.template_file.ec2_userdata.rendered}"
  iam_instance_profile        = "${aws_iam_instance_profile.bootstrap_profile.name}"
  ec2_tags                    = "${merge(var.overall_tags, var.environment_tags)}"
}
