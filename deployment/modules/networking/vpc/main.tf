resource "aws_vpc" "new_vpc" {
  cidr_block           = "${var.cidr}"
  instance_tenancy     = "${var.instance_tenancy}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_dns_support   = "${var.enable_dns_support}"

  tags = "${merge(map("Name", format("%s", var.vpc_name)), var.vpc_tags)}"
}

resource "aws_vpc_dhcp_options" "vpc_servers" {
  count               = "${var.dhcp_enabled ? 1 : 0}"
  ntp_servers         = "${var.ntp_servers}"
  domain_name         = "${var.domain_name}"
  domain_name_servers = "${var.domain_name_servers}"
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  count           = "${var.dhcp_enabled ? 1 : 0}"
  vpc_id          = "${aws_vpc.new_vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.vpc_servers.id}"
}

resource "aws_iam_role" "flow_log_role" {
  name = "${var.vpc_name}-flow-log-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "flow_log_policy" {
  name = "${var.vpc_name}-flow-log-policy"
  role = "${aws_iam_role.flow_log_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_cloudwatch_log_group" "flow_log_group" {
  name = "${var.vpc_name}-flow-log-group"
  tags = "${merge(map("Name", format("%s-flow-log-group", var.vpc_name)), var.cloudwatch_log_group_tags)}"
}

resource "aws_flow_log" "flow_logs" {
  log_group_name = "${aws_cloudwatch_log_group.flow_log_group.name}"
  iam_role_arn   = "${aws_iam_role.flow_log_role.arn}"
  vpc_id         = "${aws_vpc.new_vpc.id}"
  traffic_type   = "ALL"
}

resource "aws_default_security_group" "default_security_group" {
  vpc_id = "${aws_vpc.new_vpc.id}"
}

resource "aws_default_network_acl" "default_network_acl" {
  default_network_acl_id = "${aws_vpc.new_vpc.default_network_acl_id}"
}
