resource "aws_subnet" "restricted" {
  count = "${length(var.subnets) > 0 ? length(var.subnets) : 0}"

  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnets[count.index]}"
  availability_zone = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-%s-%s", var.subnet_name, var.network_tier, element(var.azs, count.index))), var.subnet_group_tags)}"
}

resource "aws_db_subnet_group" "restricted" {
  count = "${length(var.subnets) > 0 && var.create_subnet_database_group ? 1 : 0}"

  name        = "${lower(var.subnet_name)}"
  description = "Restricted subnet group for ${var.subnet_name}"
  subnet_ids  = ["${aws_subnet.restricted.*.id}"]

  tags = "${merge(map("Name", format("%s-db-subnet-group", var.subnet_name)), var.subnet_group_tags)}"
}
