variable "local_name" {
  description = "Local VPC's name"
}

variable "local_vpc_id" {
  description = "The current vpc's ID"
  default     = ""
}

variable "peer_owner_id" {
  description = "The AWS Account ID of the VPC that is going to be peered"
  default     = ""
}

variable "peer_name" {
  description = "Peered VPC's name"
}

variable "peer_vpc_id" {
  description = "The VPC ID that is going to be peered into"
}

variable "same_account" {
  description = "Set to true if the vpc connection is done within the same AWS account"
  default     = true
}

variable "vpc_peering_tags" {
  description = "Tags for the vpc peering"
  default     = {}
}
