resource "aws_vpc_peering_connection" "current_vpc" {
  peer_owner_id = "${var.peer_owner_id}"
  peer_vpc_id   = "${var.peer_vpc_id}"
  vpc_id        = "${var.local_vpc_id}"
  auto_accept   = "${var.same_account}"

  tags = "${merge(map("Side", format("%s-requester", var.local_name), "Name", format("%s-to-%s", var.local_name, var.peer_name)), var.vpc_peering_tags)}"
}
