resource "aws_network_acl" "aws_nacl" {
  vpc_id     = "${var.vpc_id}"
  subnet_ids = ["${var.subnet_ids}"]
  tags       = "${merge(var.nacl_tags, map("Name", format("%s", var.nacl_name)))}"
}

resource "aws_network_acl_rule" "ingress_with_cidr_blocks" {
  count          = "${length(var.ingress_with_cidr_blocks) > 0 ?  length(var.ingress_with_cidr_blocks) : 0}"
  network_acl_id = "${aws_network_acl.aws_nacl.id}"
  rule_number    = "${lookup(var.ingress_with_cidr_blocks[count.index], "rule_number")}"
  egress         = false
  protocol       = "${lookup(var.ingress_with_cidr_blocks[count.index], "protocol")}"
  rule_action    = "${lookup(var.ingress_with_cidr_blocks[count.index], "rule_action")}"
  cidr_block     = "${lookup(var.ingress_with_cidr_blocks[count.index], "cidr_block")}"
  from_port      = "${lookup(var.ingress_with_cidr_blocks[count.index], "from_port",0)}"
  to_port        = "${lookup(var.ingress_with_cidr_blocks[count.index], "to_port", 0)}"
}

resource "aws_network_acl_rule" "egress_with_cidr_blocks" {
  count          = "${length(var.egress_with_cidr_blocks) > 0 ?  length(var.egress_with_cidr_blocks) : 0}"
  network_acl_id = "${aws_network_acl.aws_nacl.id}"
  rule_number    = "${lookup(var.egress_with_cidr_blocks[count.index], "rule_number")}"
  egress         = true
  protocol       = "${lookup(var.egress_with_cidr_blocks[count.index], "protocol")}"
  rule_action    = "${lookup(var.egress_with_cidr_blocks[count.index], "rule_action")}"
  cidr_block     = "${lookup(var.egress_with_cidr_blocks[count.index], "cidr_block",)}"
  from_port      = "${lookup(var.egress_with_cidr_blocks[count.index], "from_port", 0)}"
    to_port        = "${lookup(var.egress_with_cidr_blocks[count.index], "to_port", 0)}"
}