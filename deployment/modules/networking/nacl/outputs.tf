output "network_acl_id" {
  description = "The ID of the NACL"
  value       = "${element(concat(aws_network_acl.aws_nacl.*.id, list("")), 0)}"
}
