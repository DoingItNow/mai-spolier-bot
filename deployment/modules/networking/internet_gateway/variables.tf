variable "igw_name" {
  description = "Name to be used on the igw tag"
  default     = ""
}

variable "vpc_id" {
  description = "The VPC ID"
  default     = ""
}

variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}
variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  default     = []
}

variable "public_route_table_id" {
  description = "A list of IDs of public route tables"
  default     = []
}

variable "igw_tags" {
  description = "Additional tags for the internet gateway"
  default     = {}
}
