output "subnet_ids" {
  description = "List of IDs of public subnets"
  value       = ["${aws_subnet.public.*.id}"]
}

output "subnets_list" {
  description = "List of public subnets"
  value       = "${var.subnets}"
}

output "subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = ["${aws_subnet.public.*.cidr_block}"]
}
