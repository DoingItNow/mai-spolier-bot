variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnet_name" {
  description = "Name to be used on all the resources as identifier"
}

variable "network_tier" {
  description = "The network tier where the subnet is placed"
}

variable "subnets" {
  description = "A list of subnets inside the VPC"
  default     = []
}

variable "subnet_tags" {
  description = "Additional tags for the public subnets"
  default     = {}
}

variable "map_public_ip_on_launch" {
  description = "Should be false if you do not want to auto-assign public IP on launch"
  default     = true
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}
