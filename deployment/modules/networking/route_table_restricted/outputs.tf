output "route_table_ids" {
  description = "List of IDs of restricted route tables"
  value       = ["${aws_route_table.restricted.*.id}"]
}
