variable "vpc_id" {
  description = "The VPC ID"
  default     = ""
}

variable "public_subnet_exists" {
  description = "Set to true if public subnet(s) exists"
  default     = true
}

variable "private_subnet_exists" {
  description = "Set to true if private subnet(s) exists"
  default     = true
}

variable "restricted_subnet_exists" {
  description = "Set to true if restricted subnet(s) exists"
  default     = true
}

variable "policy" {
  description = "The policy document in JSON"

  default = <<EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "*",
                "Principal": "*",
                "Resource": "*",
                "Effect": "Allow"
            }   
        ]
    }
    EOF
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  default     = []
}

variable "public_route_table_ids" {
  description = "A list of IDs of public route tables"
  default     = []
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  default     = []
}

variable "private_route_table_ids" {
  description = "A list of IDs of private route tables"
  default     = []
}

variable "restricted_subnets" {
  description = "A list of restricted subnets inside the VPC"
  default     = []
}

variable "restricted_route_table_ids" {
  description = "A list of IDs of restricted route tables"
  default     = []
}
