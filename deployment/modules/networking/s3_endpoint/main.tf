locals {
  public_endpoint_count     = "${length(var.public_subnets) > 0 ? 1 : 0}"
  private_endpoint_count    = "${length(var.private_subnets)}"
  restricted_endpoint_count = "${length(var.restricted_subnets) > 0 ? 1 : 0}"
}

data "aws_vpc_endpoint_service" "s3" {
  service = "s3"
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = "${var.vpc_id}"
  service_name = "${data.aws_vpc_endpoint_service.s3.service_name}"
  policy       = "${var.policy}"
}

resource "aws_vpc_endpoint_route_table_association" "public_s3" {
  count = "${var.public_subnet_exists ? local.public_endpoint_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${var.public_route_table_ids[0]}"
}

resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count = "${var.private_subnet_exists ? local.private_endpoint_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${element(var.private_route_table_ids, count.index)}"
}

resource "aws_vpc_endpoint_route_table_association" "restricted_s3" {
  count = "${var.restricted_subnet_exists ? local.restricted_endpoint_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${var.restricted_route_table_ids[0]}"
}
