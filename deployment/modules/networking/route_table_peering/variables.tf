variable "route_table_ids" {
  description = "A list of route table IDs"
  type        = "list"
}

variable "vpc_cidr_block" {
  description = "The Peered VPC CIDR block"
}

variable "vpc_peering_connection_id" {
  description = "the VPC Peering Connection ID"
}

variable "route_table_count" {
  description = "The length of the route table ID List"
}
