resource "aws_route" "peering_route" {
  count = "${var.route_table_count}"

  route_table_id = "${element(var.route_table_ids, count.index)}"

  destination_cidr_block    = "${var.vpc_cidr_block}"
  vpc_peering_connection_id = "${var.vpc_peering_connection_id}"
}
