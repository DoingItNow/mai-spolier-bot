resource "aws_vpc_peering_connection_accepter" "peered_vpc" {
  vpc_peering_connection_id = "${var.vpc_peering_connection_id}"
  auto_accept               = true

  tags = "${merge(map("Side", format("%s-requester", var.local_name), "Name", format("%s-to-%s", var.local_name, var.peer_name)), var.vpc_peering_tags)}"
}
