output "route_table_ids" {
  description = "List of IDs of private route tables"
  value       = ["${aws_route_table.private.*.id}"]
}

output "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  value       = "${var.enable_nat_gateway}"
}

output "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  value       = "${var.single_nat_gateway}"
}

output "nat_gateway_count" {
  description = "The number of nat gateways needed for the VPC"
  value       = "${local.nat_gateway_count}"
}
