variable "vpc_id" {
  description = "The VPC ID"
  default     = ""
}

variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}

variable "route_table_name" {
  description = "Name to be used on the route table name tag"
  default     = ""
}

variable "network_tier" {
  description = "Name of the network tier this Route Table belongs in"
}

variable "subnets" {
  description = "A list of private subnets inside the VPC"
  default     = []
}

variable "subnet_ids" {
  description = "A list of private subnets IDs inside the VPC"
  default     = []
}

variable "route_table_tags" {
  description = "Additional tags for the private route tables"
  default     = {}
}

variable "propagate_route_tables_vgw" {
  description = "Should be true if you want route table propagation"
  default     = false
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default     = false
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default     = false
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`."
  default     = false
}
