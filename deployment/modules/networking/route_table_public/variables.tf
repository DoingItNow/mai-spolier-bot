variable "vpc_id" {
  description = "The VPC ID"
}

variable "route_table_name" {
  description = "Name to be used on the route table name tag"
}

variable "network_tier" {
  description = "Name of the network tier this Route Table belongs in"
}

variable "subnets" {
  description = "A list of subnets inside the VPC"
  default     = []
}

variable "subnet_ids" {
  description = "A list of subnets IDs inside the VPC"
  default     = []
}

variable "route_table_tags" {
  description = "Additional tags for the public route tables"
  default     = {}
}

variable "propagate_route_tables_vgw" {
  description = "Should be true if you want route table propagation"
  default     = false
}

variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}
