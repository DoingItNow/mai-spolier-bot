output "route_table_ids" {
  description = "A list of IDs of route table"
  value       = "${aws_route_table.public.*.id}"
}
