resource "aws_subnet" "private" {
  count = "${length(var.subnets) > 0 ? length(var.subnets) : 0}"

  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnets[count.index]}"
  availability_zone = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-%s-%s", var.subnet_name, var.network_tier, element(var.azs, count.index))), var.subnet_tags, var.vpc_tags)}"
}
