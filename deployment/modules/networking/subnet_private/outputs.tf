output "subnet_ids" {
  description = "List of IDs of private subnets"
  value       = ["${aws_subnet.private.*.id}"]
}

output "subnets_list" {
  description = "List of private subnets"
  value       = "${var.subnets}"
}

output "subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = ["${aws_subnet.private.*.cidr_block}"]
}

output "azs" {
  description = "List Availability Zones"
  value       = "${var.azs}"
}
