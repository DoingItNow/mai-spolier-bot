# IAM Roles
variable "service_role_name" {
  type = "string"
}

variable "instance_profile_name" {
  type = "string"
}

variable "instance_profile_policy_name" {
  type = "string"
}

variable "instance_profile_policy_document" {
  type = "string"
}

variable "instance_profile_managed_policy_name" {
  type = "string"
}

variable "service_role_policy_name" {
  type = "string"
}

variable "service_role_policy_document" {
  type = "string"
}

# Security Groups
variable "master_security_group_tags" {
  type    = "map"
  default = {}
}

variable "slave_security_group_tags" {
  type    = "map"
  default = {}
}

variable "service_access_security_group_tags" {
  type    = "map"
  default = {}
}

# EMR Cluster
variable "cluster_name" {
  type = "string"
}

variable "vpc_id" {
  type = "string"
}

variable "release_label" {
  type = "string"
}

variable "applications" {
  type    = "list"
  default = ["Spark"]
}

variable "configurations" {
  type = "string"
}

variable "key_name" {
  type = "string"
}

variable "subnet_id" {
  type = "string"
}

variable "instance_groups" {
  type = "list"
}

variable "ebs_root_volume_size" {
  type = "string"
}

variable "bootstrap_name" {
  type = "string"
}

variable "bootstrap_uri" {
  type = "string"
}

variable "bootstrap_args" {
  type    = "list"
  default = []
}

variable "post_bootstrap_step_name" {
  type = "string"
}

variable "post_bootstrap_step_jar" {
  type = "string"
}

variable "post_bootstrap_step_args" {
  type    = "list"
  default = []
}

variable "log_uri" {
  type = "string"
}

variable "bastion_access_sg" {
  type = "string"
}

variable "shared_services_cidr" {
  type = "string"
}

variable "custom_ami_id" {
  type = "string"
}

variable "emr_cluster_tags" {
  type    = "map"
  default = {}
}

variable "s3_cert_object" {
  type = "string"
}

variable "emr_sns_alert_topic_arn" {
  type = "string"
}
