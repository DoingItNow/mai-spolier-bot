variable "bucket_name" {
  description = "Name of the bucket"
  type        = "string"
}

variable "bucket_policy" {
  description = "JSON formatted bucket policy attached to the bucket"
  type        = "string"
  default     = ""
}

variable "logging_bucket" {
  description = "The S3 bucket to send S3 access logs"
  type        = "string"
}

variable "bucket_tags" {
  description = "Mapping of tags to assign to the bucket"
  type        = "map"
  default     = {}
}

variable "kms_key_arn" {
  description = "KMS Key ARN we will encrypt the bucket with"
  type        = "string"
}

variable "transition_prefix" {
  description = "Prefix that the current transition applies to"
  type        = "string"
  default     = ""
}

variable "abort_incomplete_multipart_upload_days" {
  description = "Days before deleting failed multipart upload files"
  type        = "string"
  default     = 14
}

variable "noncurrent_ia_transition_days" {
  description = "Number of days before transitioning noncurrent objects to STANDARD_IA"
  type        = "string"
  default     = 365
}

variable "noncurrent_version_expiration_days" {
  description = "Number of days before expiring an old object version"
  type        = "string"
  default     = 2555
}

variable "current_ia_transition_days" {
  description = "Number of days before transitioning current object to STANDARD_IA"
  type        = "string"
  default     = 2500
}

variable "current_glacier_transition_days" {
  description = "Number of days before transitioning current objects to GLACIER"
  type        = "string"
  default     = 2555
}
