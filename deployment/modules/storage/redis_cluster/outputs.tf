output "redis_subnet_group_name" {
  value = "${aws_elasticache_subnet_group.redis_subnet_group.name}"
}

output "id" {
  value = "${aws_elasticache_replication_group.redis.id}"
}

output "port" {
  value = "${var.redis_port}"
}

output "endpoint" {
  value = "${aws_elasticache_replication_group.redis.configuration_endpoint_address}"
}
