resource "aws_elasticache_replication_group" "redis" {
  engine         = "redis"
  engine_version = "${var.redis_version}"

  replication_group_id          = "${var.name}"
  replication_group_description = "Redis Cluster for ${var.name}"

  at_rest_encryption_enabled = true

  # Airflow/Celery broker_url setting doesn't support Redis connections over TLS
  # See https://github.com/celery/celery/issues/4694
  transit_encryption_enabled = false

  node_type = "${var.redis_node_type}"
  port      = "${var.redis_port}"

  parameter_group_name = "${var.parameter_group_name}"
  subnet_group_name    = "${aws_elasticache_subnet_group.redis_subnet_group.id}"
  security_group_ids   = ["${var.security_group_ids}"]

  apply_immediately        = "${var.apply_immediately}"
  maintenance_window       = "${var.redis_maintenance_window}"
  snapshot_window          = "${var.redis_snapshot_window}"
  snapshot_retention_limit = "${var.redis_snapshot_retention_limit}"

  number_cache_clusters      = "${var.number_cache_clusters}"
  automatic_failover_enabled = true

  tags = "${var.tags}"
}

resource "aws_elasticache_subnet_group" "redis_subnet_group" {
  name       = "${replace(format("%.255s", lower(replace(var.name, "_", "-"))), "/\\s/", "-")}"
  subnet_ids = ["${var.subnet_ids}"]
}
