output "id" {
  description = "The ID that identifies the file system (e.g. fs-ccfc0d65)."
  value       = "${aws_efs_file_system.efs.id}"
}
