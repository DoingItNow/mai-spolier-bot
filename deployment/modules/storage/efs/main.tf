resource "aws_efs_file_system" "efs" {
  creation_token  = "${var.creation_token}"
  kms_key_id      = "${var.kms_key_id}"
  tags            = "${var.efs_tags}"
  encrypted       = "${var.encrypted}"
}

resource "aws_efs_mount_target" "efs_target" {
  count           = "${length(var.subnet_ids) > 0 ? length(var.subnet_ids) : 0}"
  file_system_id  = "${aws_efs_file_system.efs.id}"
  subnet_id       = "${element(var.subnet_ids, count.index)}"
  security_groups = ["${var.security_groups}"]
}

