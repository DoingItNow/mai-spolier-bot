variable "bucket_name" {
  description = "Name of the bucket"
  type        = "string"
}

variable "bucket_policy" {
  description = "JSON formatted bucket policy attached to the bucket"
  type        = "string"
  default     = ""
}

variable "bucket_tags" {
  description = "Mapping of tags to assign to the bucket"
  type        = "map"
  default     = {}
}

variable "abort_incomplete_multipart_upload_days" {
  description = "Days before deleting failed multipart upload files"
  type        = "string"
  default     = 14
}

variable "noncurrent_ia_transition_days" {
  description = "Number of days before transitioning objects to STANDARD_IA"
  type        = "string"
  default     = 365
}

variable "noncurrent_version_expiration_days" {
  description = "Number of days before expiring an old object version"
  type        = "string"
  default     = 2555
}

variable "current_ia_transition_days" {
  description = "Number of days before transitioning current object to STANDARD_IA"
  type        = "string"
  default     = 2500
}

variable "current_glacier_transition_days" {
  description = "Number of days before transitioning current objects to GLACIER"
  type        = "string"
  default     = 2555
}
