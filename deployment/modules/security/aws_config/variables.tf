variable "s3_bucket_name" {
  description = "bucket used for logging"
}

variable "config_role" {
  description = "role for aws config"
}

variable "s3_key_prefix" {
  description = "prefix for the config logs"
}
