# Module for enabling aws config

resource "aws_config_configuration_recorder" "aws-config" {
  name     = "aws-config"
  role_arn = "${var.config_role}"
}

resource "aws_config_configuration_recorder_status" "aws-config" {
  name       = "${aws_config_configuration_recorder.aws-config.name}"
  is_enabled = true
  depends_on = ["aws_config_delivery_channel.aws-config-delivery-channel"]
}

resource "aws_config_delivery_channel" "aws-config-delivery-channel" {
  name           = "aws-config-delivery-channel"
  s3_bucket_name = "${var.s3_bucket_name}"
  s3_key_prefix  = "${var.s3_key_prefix}"
  depends_on     = ["aws_config_configuration_recorder.aws-config"]
}
