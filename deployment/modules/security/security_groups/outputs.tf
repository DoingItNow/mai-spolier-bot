# Resource taken from: https://github.com/terraform-aws-modules/terraform-aws-security-group
output "security_group_id" {
  description = "The ID of the security group"
  value       = "${element(concat(aws_security_group.aws_sg.*.id, list("")), 0)}"
}

output "security_group_vpc_id" {
  description = "The VPC ID"
  value       = "${element(concat(aws_security_group.aws_sg.*.vpc_id, list("")), 0)}"
}

output "security_group_owner_id" {
  description = "The owner ID"
  value       = "${element(concat(aws_security_group.aws_sg.*.owner_id, list("")), 0)}"
}

output "security_group_name" {
  description = "The name of the security group"
  value       = "${element(concat(aws_security_group.aws_sg.*.name, list("")), 0)}"
}

output "security_group_description" {
  description = "The description of the security group"
  value       = "${element(concat(aws_security_group.aws_sg.*.description, list("")), 0)}"
}
