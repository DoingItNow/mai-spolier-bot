#!/bin/bash

cd /home/ubuntu

sudo apt-get update
sudo apt-get install unzip mono-complete python3.6 python3-pip python-pip make screen awscli -y

pip install awscli
pip3 install awscli

aws s3 cp s3://discord-spoiler-bot-artefacts/spoiler-bot.zip spoiler-bot.zip 
unzip spoiler-bot.zip 

make install_packages

DISCORD_TOKEN=$(aws secretsmanager get-secret-value --secret-id discord-spoiler-bot-token --region ap-southeast-2 --query SecretString --output text | awk -F'"' '{print $4}') PASTEBIN_API_KEY=$(aws secretsmanager get-secret-value --secret-id discord-bot-pastebin-api-key --region ap-southeast-2 --query SecretString --output text | awk -F'"' '{print $4}') nohup python3 src/main.py > my_discord_bot.txt 2>&1 &
