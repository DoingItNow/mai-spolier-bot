terragrunt = {
  remote_state {
    backend = "s3"

    config {
      bucket         = "im-cicd-artifacts-${get_aws_account_id()}"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "ap-southeast-2"
      encrypt        = true
      dynamodb_table = "terraform-lock-table"
    }
  }
}

overall_tags = {}

overall_asg_tags = []
