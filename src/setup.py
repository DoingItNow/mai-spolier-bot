"""
Module to get the bot details to be logged into consol
"""

def get_client_details(discord_client):
    """
        Retunrs the clien user name and ID
    """
    client_user_array = [discord_client.user.name, discord_client.user.id]
    return client_user_array
