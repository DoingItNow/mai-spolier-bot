"""
Creates spoiler embedded messages
"""
import os
import urllib.parse
import discord
from pbwrap import Pastebin


def create_pastebin_link(text):
    """
        Creates text in pastebin and returns the URL
    """
    pastebin_class = Pastebin(os.environ.get('PASTEBIN_API_KEY'))
    pastebin_url = pastebin_class.create_paste(text, api_paste_private=0, api_paste_name=None,
                                               api_paste_expire_date="1M", api_paste_format=None)
    return pastebin_url


def encode_spoiler_text(text):
    """
        Converts text into a URL encoded message
    """
    encoded_text = urllib.parse.quote_plus(text)
    return encoded_text


def split_title_and_spoiler(text):
    """
        If the | is used, split the string to a list
    """
    message_list = []
    if "|" in text:
        message_list = text.split(" | ")
    return message_list


async def create_spoiler_message(bot_command, spoiler_text, user):
    """
        Sends a embedded message that has a hover link as a spoiler
    """

    spoiler_text_list = split_title_and_spoiler(spoiler_text)

    if spoiler_text_list:
        title_of_spoiler = spoiler_text_list[0]
        body_of_spoiler = spoiler_text_list[1]

        pastelink_url = create_pastebin_link(body_of_spoiler)

        encoded_text = encode_spoiler_text(body_of_spoiler)
        embedded_message = discord.Embed(colour=0x8D15C4)
        embedded_message.title = "Spoiler Incoming for " + \
            title_of_spoiler + " from " + str(user)
        embedded_message.description = '[Hover to View](https://dummyimage.com/9086x400/000/ffffff&text=' + \
            encoded_text + ' "' + body_of_spoiler + '")\n\n' + \
            '[View Pastebin Link](' + pastelink_url + ')'
    else:
        encoded_text = encode_spoiler_text(spoiler_text)
        pastelink_url = create_pastebin_link(spoiler_text)

        embedded_message = discord.Embed(colour=0x8D15C4)
        embedded_message.title = "Spoiler Incoming from " + str(user)
        embedded_message.description = '[Hover to View](https://dummyimage.com/9086x400/000/ffffff&text=' + \
            encoded_text + ' "' + spoiler_text + '")\n\n' + \
            '[View Pastebin Link](' + pastelink_url + ')'

    await bot_command.say(embed=embedded_message)
