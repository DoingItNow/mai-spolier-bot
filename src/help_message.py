"""
Creates help message
"""
import discord

async def send_help_response(bot_command):
    """
        Sends a embedded message in how to use the bot
    """
    embedded_message = discord.Embed(colour=0x8D15C4)
    embedded_message.title = ""
    field_name = "Mai Spoiler Bot Help"
    field_value = ('Hello! I put spoilers into an embedded message. #nospoilerinopls\n'
                   'The two commands available are as follows:\n'
                   '```!spoiler [insert spoiler text]\n'
                   'Example: !spoiler This is a spoiler\n\n'
                   '!spoiler [name of anime title] | [insert spoiler text]\n'
                   'Example: !spoiler Random Anime | This is a spoiler```\n\n')

    embedded_message.add_field(name=field_name, value=field_value)
    await bot_command.say(embed=embedded_message)
