"""
Unit tests for setup.py
"""
import unittest
from src import spoiler_message

class SpoilerTests(unittest.TestCase):
    """Class that defines all the test cases
    """
    def test_split_title_text(self):
        """Test to see if the title and body is split properly
        """
        expected_list = ["Random Title", "Random Text"]
        actual_list = spoiler_message.split_title_and_spoiler("Random Title | Random Text")
        self.assertEqual(expected_list, actual_list)
