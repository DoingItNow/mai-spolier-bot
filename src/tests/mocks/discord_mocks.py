"""
Mock classes used for unit testing
"""
class MockChannel():
    """
    Class definition to create a mocked channel
    """
    def __init__(self, channel_name):
        """Initial values when a MockChannel class is created.

        :param channel_name: A string that is the name of the channel
        """
        self.name = channel_name

class MockUser():
    """
    Class definition to create a mocked user
    """
    def __init__(self, bot_name="Bot_Name", user_id="0987654321"):
        """Initial values when a MockUser class is created.

        :param bot_name: A string that has the bot's name
        :param user_id: A string that is the user ID
        """
        self.name = bot_name
        self.mention = "@" + self.name
        self.id = user_id
        self.server = MockServer()

class MockServer():
    """
    Class definition to create a mocked server
    """
    def __init__(self, server_id="111111111111232321"):
        """Initial values when a MockServer class is created.

        :param server_id: A string that is the server ID
        """
        self.id = server_id
        self.channels = setup_mock_channels_list()

class MockBot():
    """
    Class definition to create a mocked bot
    """
    def __init__(self, bot_name):
        """Initial values when a MockServer class is created.

        :param bot_name: A string that is the name of the bot
        """
        self.user = MockUser(bot_name)
        self.servers = setup_mock_servers_id_list()

def setup_mock_channels_list():
    """
    Creates a list of mock channel objects
    """
    channels_array = []
    channel_1 = MockChannel("team-kudzu")
    channel_2 = MockChannel("team-kuzu")
    channel_3 = MockChannel("team-udzu")
    channel_3 = MockChannel("team-ad-bleak-steak")
    channel_4 = MockChannel("test_channel")
    channels_array.append(channel_1)
    channels_array.append(channel_2)
    channels_array.append(channel_3)
    channels_array.append(channel_4)

    return channels_array

def setup_mock_servers_id_list():
    """
    Creates a list of mock server objects
    """
    servers_array = []
    server_1 = MockServer("116434225233788934")
    server_2 = MockServer("116178177533018119")
    server_3 = MockServer("222222222222222222")
    server_4 = MockServer("343434334232323223")
    servers_array.append(server_1)
    servers_array.append(server_2)
    servers_array.append(server_3)
    servers_array.append(server_4)

    return servers_array
