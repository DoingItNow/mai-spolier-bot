"""
Main module for Kasmeer Bot
"""

import discord
from discord.ext import commands
import setup
import spoiler_message
import help_message
from config import config

bot = commands.Bot(command_prefix='!')
bot.remove_command("help")

CLIENT = discord.Client()

@bot.event
async def on_ready():
    """
        Function that calls client is done preparing the data received from Discord
    """

    print('Logged in as')
    client_array = setup.get_client_details(bot)
    print(client_array[0])
    print('------')
    await bot.change_presence(game=discord.Game(name='!spoiler [insert your spoilers here]'))

@bot.command(pass_context=True)
async def spoiler(ctx, *, spoiler_text: str):
    """
        Function that calls to assign a colour for the user
    """

    message = ctx.message

    # Get the specific variables from the message
    user = message.author
    await bot.delete_message(message)
    await spoiler_message.create_spoiler_message(bot, spoiler_text, user)

@bot.command(pass_context=True)
async def help(ctx):
    """
        Function that gives help message
    """
    message = ctx.message
    await bot.delete_message(message)
    await help_message.send_help_response(bot)

bot.run(config.__token__)
