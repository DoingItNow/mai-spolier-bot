# Spoiler Bot

## How to run
In order to run this bot locally you need the following:
* Python 3.6+

## Install Dependencies
### Windows
Run the following command: `pip install -r requirements.txt`

### Mac/Linux
Run the command: `make install_packages`

## Set bot locally
Get the discord token and save it as the environment variable `DISCORD_TOKEN`

Run `python main.py` or `make bot-run`

## How to authorise bot to join your server
Get the client ID of the bot and paste it into the URL `https://discordapp.com/oauth2/authorize?client_id=529075624485322782&scope=bot&permissions=1073870864` where `CLIENTID` is the client_id

## Run the tests
To run unit tests use `python -m unittest tests`

For Mac/Linux use `make tests`

## Looking at the Bot logs in the EC2
SSH into the box and run screen -r to hook back into the shell session