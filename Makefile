.PHONY: test phpunit pyunit
CURRENT_DIRECTORY=$(shell pwd)
CONTAINER_NAME:=kasmeer_bot_tests_container_$(shell uuidgen)
IMAGE_NAME:=kasmeer_bot_tests$(shell uuidgen | tr "[:upper:]" "[:lower:]")

tests: style_check pyunit

install_packages:
	pip3 install -r requirements.txt

pyunit:
	PYTHONPATH=$(CURRENT_DIRECTORY)/src python3 -m unittest tests

style_check:
	PYTHONPATH=$(CURRENT_DIRECTORY)/src python3 -B -m pylint --rcfile=src/tests/pylintrc tests

bot-run:
	python3 src/main.py
